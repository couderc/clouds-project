import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore'
import { User } from './signin/user.model';
import { Router, RouterPreloader } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { CovidSummary } from './CovidSummary.model';
import { CountriesData } from './countriesData.model';
import { SpecificDaily } from './SpecificDaily.model';
import { News } from './news.model';
import { AllCountriesDaily } from './AllCountriesDaily.model';

@Injectable({
  providedIn: 'root'
})
export class CoronavirusService {
  private user: User;
  covidSummary: CovidSummary;
  countriesData: CountriesData[];
  specificDailyRaw: SpecificDaily[];
  allCountriesDaily: AllCountriesDaily;

  constructor(private afAuth: AngularFireAuth, private router: Router,
    private firestore: AngularFirestore, private http: HttpClient) { }

  async signInWithGoogle() {
    const credientials = await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    this.user = {
      uid: credientials.user.uid,
      displayName: credientials.user.displayName,
      email: credientials.user.email
    };
    localStorage.setItem("user", JSON.stringify(this.user));
    this.updateUserData();
    this.router.navigate(["covid"]);

  }
  private updateUserData() {
    this.firestore.collection("users").doc(this.user.uid).set({
      uid: this.user.uid,
      displayName: this.user.displayName,
      email: this.user.email

    }, { merge: true });
  }
  getUser() {
    console.log("User Firestore Access")
    if (this.user == null && this.userSignedIn()) {
      this.user = JSON.parse(localStorage.getItem("user"));
    }
    return this.user
  }

  userSignedIn(): boolean {
    return JSON.parse(localStorage.getItem("user")) != null;
  }

  signOut() {
    this.afAuth.signOut();
    localStorage.removeItem("user");
    this.user = null;
    this.router.navigate(["signin"]);

  }



  apiRequestSummary() {
    if (!this.updatedSummary){
    this.http.get<JSON>("https://api.covid19api.com/summary").subscribe(data => {
      console.log(data);

      this.covidSummary = {
        newCases: data['Global']['NewConfirmed'],
        totalCases: data['Global']['TotalConfirmed'],
        activeCases: data['Global']['TotalConfirmed'] - ((data['Global']['TotalDeaths']) + (data['Global']['TotalRecovered'])),
        totalRecovered: data['Global']['TotalRecovered'],
        newRecovered: data['Global']['NewRecovered'],
        recoveryRate: Math.round((10000 * data['Global']['TotalRecovered']) / (data['Global']['TotalConfirmed'])) / 100,
        totalDeaths: data['Global']['TotalDeaths'],
        newDeaths: data['Global']['NewDeaths'],
        mortalityRate: Math.round((10000 * data['Global']['TotalDeaths']) / (data['Global']['TotalConfirmed'])) / 100,
        Date: new Date(data['Date']),
      }
      this.countriesData = data["Countries"]
      console.log(this.countriesData)
      this.updateCovidSummary();
      this.updateCountriesData();
    })
  }else{
    console.log("SummaryUpToDate")
  }
  }
  updatedSummary(){
    this.firestore.collection("summary").doc("summary").valueChanges().subscribe((summary) => {
      var i = (new Date(summary["date"])).getMonth()*100 + (new Date(summary["date"])).getDate()
      var j = (new Date().getMonth())*100 + (new Date()).getDate()
      return (i == j)
    })
    return false
  }


  getCovidSummary() {
    console.log("CovidSummary Firestore Access")
    return this.firestore.collection('summary').doc('summary').valueChanges()

  }
  private updateCovidSummary() {
    this.firestore.collection('summary').doc('summary').set({
      totalCases: this.covidSummary.totalCases,
      newCases: this.covidSummary.newCases,
      activeCases: this.covidSummary.activeCases,
      totalRecovered: this.covidSummary.totalRecovered,
      newRecovered: this.covidSummary.newRecovered,
      recoveryRate: this.covidSummary.recoveryRate,
      totalDeaths: this.covidSummary.totalDeaths,
      newDeaths: this.covidSummary.newDeaths,
      mortalityRate: this.covidSummary.mortalityRate,
      Date: this.covidSummary.Date,


    }, { merge: true });
  }
  getCountriesData() {
    console.log("CountriesData Firestore Access")
    return this.firestore.collection("countries").valueChanges();
  }

  updateCountriesData() {
    this.countriesData.forEach(country => {
      if (country != null){
      this.firestore.collection("countries").doc(country.Slug).set({
        Country: country.Country,
        CountryCode: country.CountryCode,
        Slug: country.Slug,
        NewConfirmed: country.NewConfirmed,
        TotalConfirmed: country.TotalConfirmed,
        NewDeaths: country.NewDeaths,
        TotalDeaths: country.TotalDeaths,
        NewRecovered: country.NewRecovered,
        TotalRecovered: country.TotalRecovered,
        Date: country.Date,
      }, { merge: true });
    }}
    );
  }

  apiRequestCountry(slug: string) {
    this.http.get<SpecificDaily[]>("https://api.covid19api.com/total/country/" + slug).subscribe(data => {
      console.log(data);
      this.specificDailyRaw = data;
    });
    this.updateSpecificDaily(slug);

  }
  getSpecificDaily(slug: string) {
    console.log("SpecificDaily Firestore Access")
    return this.firestore.collection("countries").doc(slug).collection("daily").valueChanges()
  }

  updateSpecificDaily(slug: string) {
    this.specificDailyRaw.forEach((day: SpecificDaily) => {
      if (day != null){
      this.firestore.collection("countries").doc(slug).collection("daily").doc(day.Date).set({
        Confirmed: day.Confirmed,
        Deaths: day.Deaths,
        Recovered: day.Recovered,
        Active: day.Active,
        Date: day.Date,
      }, { merge: true });
    }}
    );
  }
  getNews() {
    console.log("CountriesData Firestore Access")
    return this.firestore.collection("news").valueChanges();
  }
  addNews(news: News) {
    this.firestore.collection("news").add(news);
  }

  apiRequestAllCountriesDaily() {
    this.http.get<JSON>("https://corona.lmao.ninja/v2/historical/all").subscribe(data => {
      console.log(data);
      this.allCountriesDaily = {
        Date: Object.keys(data["cases"]),
        cases: Object.values(data['cases']),
        deaths: Object.values(data['deaths']),
        recovered: Object.values(data['recovered']),
      }
      console.log(this.allCountriesDaily);
      this.updateAllCountriesDaily();
    });
  }

  getAllCountriesDaily() {
    console.log("AllCountriesDaily Firestore Access")
    return this.firestore.collection("daily").doc("daily").valueChanges();
    
  }

  updateAllCountriesDaily() {
    console.log(this.allCountriesDaily.Date)
    this.firestore.collection("daily").doc("daily").set({
      Date: this.allCountriesDaily.Date,
      cases: this.allCountriesDaily.cases,
      deaths: this.allCountriesDaily.deaths,
      recovered: this.allCountriesDaily.recovered,
    }, { merge: true });
    console.log("updateAllCountries")
  }

  getAllowedUsers() {
    console.log("AllowedUsers Firestore Access")
    return this.firestore.collection("allowedUsers").valueChanges();
    
  }
  addAllowedUsers(user : User){
    this.firestore.collection("allowedUsers").add(user);
  }
  
}
