import { Component, OnInit } from '@angular/core';
import { CoronavirusService } from '../coronavirus.service';
import { CountriesData } from '../countriesData.model';
import { SpecificDaily } from '../SpecificDaily.model';


@Component({
  selector: 'app-specific-daily',
  templateUrl: './specific-daily.component.html',
  styleUrls: ['./specific-daily.component.css']
})
export class SpecificDailyComponent implements OnInit {
  specificDaily : SpecificDaily[];
  
  constructor(public coronavirusService : CoronavirusService) { }

  ngOnInit(): void {

  }

}
