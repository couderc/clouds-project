import { Component, OnInit } from '@angular/core';
import { CoronavirusService } from '../coronavirus.service';
import { News } from '../news.model';
import { FormsModule } from '@angular/forms';
import { CountriesData } from '../countriesData.model';
import { User } from '../signin/user.model';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  done : boolean = false;
  news : News;
  title : string;
  country : string;
  Date : Date ;
  description : string;
  imageURL : string;
  readMore : string;
  user : User;
  eligibleUser : Boolean = false;
  countryList: [{value: 'afghanistan'},{value: 'albania'},{value: 'algeria'},{value: 'andorra'},{value: 'angola'},{value: 'antigua-and-barbuda'},{value: 'argentina'},{value: 'armenia'},{value: 'australia'},{value: 'austria'},{value: 'azerbaijan'},{value: 'bahamas'},{value: 'bahrain'},{value: 'bangladesh'},{value: 'barbados'},{value: 'belarus'},{value: 'belgium'},{value: 'belize'},{value: 'benin'},{value: 'bhutan'},{value: 'bolivia'},{value: 'bosnia-and-herzegovina'},{value: 'botswana'},{value: 'brazil'},{value: 'brunei'},{value: 'bulgaria'},{value: 'burkina-faso'},{value: 'burundi'},{value: 'cambodia'},{value: 'cameroon'},{value: 'cape-verde'},{value: 'central-african-republic'},{value: 'chad'},{value: 'chile'},{value: 'china'},{value: 'colombia'},{value: 'comoros'},{value: 'congo-brazzaville'},{value: 'congo-kinshasa'},{value: 'costa-rica'},{value: 'croatia'},{value: 'cuba'},{value: 'cyprus'},{value: 'czech-republic'},{value: 'cote-divoire'},{value: 'denmark'},{value: 'djibouti'},{value: 'dominica'},{value: 'dominican-republic'},{value: 'ecuador'},{value: 'egypt'},{value: 'el-salvador'},{value: 'equatorial-guinea'},{value: 'eritrea'},{value: 'estonia'},{value: 'ethiopia'},{value: 'fiji'},{value: 'finland'},{value: 'france'},{value: 'gabon'},{value: 'gambia'},{value: 'georgia'},{value: 'germany'},{value: 'ghana'},{value: 'greece'},{value: 'grenada'},{value: 'guatemala'},{value: 'guinea'},{value: 'guinea-bissau'},{value: 'guyana'},{value: 'haiti'},{value: 'holy-see-vatican-city-state'},{value: 'honduras'},{value: 'hungary'},{value: 'iceland'},{value: 'india'},{value: 'indonesia'},{value: 'iran'},{value: 'iraq'},{value: 'ireland'},{value: 'israel'},{value: 'italy'},{value: 'jamaica'},{value: 'japan'},{value: 'jordan'},{value: 'kazakhstan'},{value: 'kenya'},{value: 'korea-south'},{value: 'kuwait'},{value: 'kyrgyzstan'},{value: 'lao-pdr'},{value: 'latvia'},{value: 'lebanon'},{value: 'lesotho'},{value: 'liberia'},{value: 'libya'},{value: 'liechtenstein'},{value: 'lithuania'},{value: 'luxembourg'},{value: 'macedonia'},{value: 'madagascar'},{value: 'malawi'},{value: 'malaysia'},{value: 'maldives'},{value: 'mali'},{value: 'malta'},{value: 'marshall-islands'},{value: 'mauritania'},{value: 'mauritius'},{value: 'mexico'},{value: 'moldova'},{value: 'monaco'},{value: 'mongolia'},{value: 'montenegro'},{value: 'morocco'},{value: 'mozambique'},{value: 'myanmar'},{value: 'namibia'},{value: 'nepal'},{value: 'netherlands'},{value: 'new-zealand'},{value: 'nicaragua'},{value: 'niger'},{value: 'nigeria'},{value: 'norway'},{value: 'oman'},{value: 'pakistan'},{value: 'palestine'},{value: 'panama'},{value: 'papua-new-guinea'},{value: 'paraguay'},{value: 'peru'},{value: 'philippines'},{value: 'poland'},{value: 'portugal'},{value: 'qatar'},{value: 'kosovo'},{value: 'romania'},{value: 'russia'},{value: 'rwanda'},{value: 'saint-kitts-and-nevis'},{value: 'saint-lucia'},{value: 'saint-vincent-and-the-grenadines'},{value: 'samoa'},{value: 'san-marino'},{value: 'sao-tome-and-principe'},{value: 'saudi-arabia'},{value: 'senegal'},{value: 'serbia'},{value: 'seychelles'},{value: 'sierra-leone'},{value: 'singapore'},{value: 'slovakia'},{value: 'slovenia'},{value: 'solomon-islands'},{value: 'somalia'},{value: 'south-africa'},{value: 'south-sudan'},{value: 'spain'},{value: 'sri-lanka'},{value: 'sudan'},{value: 'suriname'},{value: 'swaziland'},{value: 'sweden'},{value: 'switzerland'},{value: 'syria'},{value: 'taiwan'},{value: 'tajikistan'},{value: 'tanzania'},{value: 'thailand'},{value: 'timor-leste'},{value: 'togo'},{value: 'trinidad-and-tobago'},{value: 'tunisia'},{value: 'turkey'},{value: 'uganda'},{value: 'ukraine'},{value: 'united-arab-emirates'},{value: 'united-kingdom'},{value: 'united-states'},{value: 'uruguay'},{value: 'uzbekistan'},{value: 'vanuatu'},{value: 'venezuela'},{value: 'vietnam'},{value: 'yemen'},{value: 'zambia'},{value: 'zimbabwe'}];

  
  constructor(public coronavirusService: CoronavirusService){}

  ngOnInit(): void { 
    if (!this.done){
      this.user = this.coronavirusService.getUser();
      this.coronavirusService.getAllowedUsers().subscribe((data : User[]) => {
        for(let elUser of data){
          if(elUser.email == this.user.email){
            this.eligibleUser = true;
          }
        }

      } )
      this.done=true
    }
  }

  addNews(){
    this.news = {
      title : this.title,
      Date : new Date(this.Date),
      description : this.description,
      imageURL : this.imageURL,
      readMore : this.readMore,
      country : this.country,
      user : this.user,
    };
    this.coronavirusService.addNews(this.news);
    this.title = undefined;
    this.Date = undefined;
    this.description = undefined;
    this.imageURL = undefined;
    this.readMore = undefined;

  }
  

}
