import { Component, OnInit } from '@angular/core';
import { CovidSummary } from '../CovidSummary.model';
import { CoronavirusService } from '../coronavirus.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  covidSummary: CovidSummary;
  done: Boolean = false;


  constructor(public coronavirusService: CoronavirusService) { }

  ngOnInit(): void {
    if (!this.done) {
      this.coronavirusService.getCovidSummary().subscribe((summary: CovidSummary[]) => {
        this.covidSummary = {
          newCases: summary['newCases'],
          totalCases: summary['totalCases'],
          activeCases: summary['activeCases'],
          totalRecovered: summary['totalRecovered'],
          newRecovered: summary['newRecovered'],
          recoveryRate: summary['recoveryRate'],
          totalDeaths: summary['totalDeaths'],
          newDeaths: summary['newDeaths'],
          mortalityRate: summary['mortalityRate'],
          Date: new Date(summary['Date']),
        }

      });
      this.done = true
    }

  }

}
