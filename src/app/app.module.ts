import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from "@angular/common";
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { SigninComponent } from './signin/signin.component';
import { CovidComponent } from './covid/covid.component';
import { ChartsModule } from 'ng2-charts';
import { SummaryComponent } from './summary/summary.component';
import { PiechartComponent } from './piechart/piechart.component';
import { BarchartComponent } from './barchart/barchart.component';
import { LinechartComponent } from './linechart/linechart.component';
import { HttpClientModule } from '@angular/common/http';
import { CountriesComponent } from './countries/countries.component';
import { CovidCountryComponentComponent } from './covid-country-component/covid-country-component.component';
import { SpecificDailyComponent } from './specific-daily/specific-daily.component';
import { FormsModule } from '@angular/forms';
import { NewsComponent } from './news/news.component';
import { NewsDisplayComponent } from './news-display/news-display.component';
import { NewsCountryDispayComponent } from './news-country-dispay/news-country-dispay.component';
import { AllowedUsersComponent } from './allowed-users/allowed-users.component';


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    CovidComponent,
    SummaryComponent,
    PiechartComponent,
    BarchartComponent,
    LinechartComponent,
    CountriesComponent,
    CovidCountryComponentComponent,
    SpecificDailyComponent,
    NewsComponent,
    NewsDisplayComponent,
    NewsCountryDispayComponent,
    AllowedUsersComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    ChartsModule,
    HttpClientModule,
    FormsModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


