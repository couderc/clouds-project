import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsCountryDispayComponent } from './news-country-dispay.component';

describe('NewsCountryDispayComponent', () => {
  let component: NewsCountryDispayComponent;
  let fixture: ComponentFixture<NewsCountryDispayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsCountryDispayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsCountryDispayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
