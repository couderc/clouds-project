import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CoronavirusService } from '../coronavirus.service';
import { News } from '../news.model';

@Component({
  selector: 'app-news-country-dispay',
  templateUrl: './news-country-dispay.component.html',
  styleUrls: ['./news-country-dispay.component.css']
})
export class NewsCountryDispayComponent implements OnInit {

  slug: string;
  newsL: Array<News> = [];
  done: boolean = false

  constructor(public coronavirusService: CoronavirusService,private router : Router) { }

  ngOnInit(): void {
    var n = this.router.url.toString().split('/').length
    this.slug = this.router.url.toString().split('/')[n-1];
    this.coronavirusService.getNews().subscribe((data: News[]) => {
      for(let news of data){
        if(news.country == this.slug && !this.done){
          this.newsL.push(news)
        }
      }
      this.done = true
    });
  }


}
