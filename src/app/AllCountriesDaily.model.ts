export interface AllCountriesDaily{
    cases : Array<number>;
    deaths : Array<number>;
    recovered : Array<number>;
    Date : Array<string>;
}
