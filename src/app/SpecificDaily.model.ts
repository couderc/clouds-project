export interface SpecificDaily{
    Confirmed : number;
    Deaths : number;
    Recovered : number;
    Active : number;
    Date : string ;
}
