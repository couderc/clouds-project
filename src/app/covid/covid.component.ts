import { Component, OnInit } from '@angular/core';
import { CoronavirusService } from '../coronavirus.service';
import { News } from '../news.model';
import { User } from '../signin/user.model';

@Component({
  selector: 'app-covid',
  templateUrl: './covid.component.html',
  styleUrls: ['./covid.component.css']
})
export class CovidComponent implements OnInit {
  user: User;
  done : Boolean = false;
  news : News[];

  constructor(public coronavirusService: CoronavirusService) { }

  ngOnInit(): void {
    if(!this.done){
      this.user = this.coronavirusService.getUser();
      this.done = true
    }
  }

}
