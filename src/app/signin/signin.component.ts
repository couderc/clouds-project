import { Component, OnInit } from '@angular/core';
import { CoronavirusService } from '../coronavirus.service';
import { CovidSummary } from '../CovidSummary.model';
import { CountriesData } from '../countriesData.model';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  covidSummary : CovidSummary;
  done : Boolean = false;

  constructor(public coronavirusService: CoronavirusService) { }

  ngOnInit(): void {
    if (!this.done){
    this.coronavirusService.apiRequestSummary() 
    this.coronavirusService.apiRequestAllCountriesDaily()
    this.done = true
  }
    //this.coronavirusService.apiRequestCountries()
 
    
}
}
