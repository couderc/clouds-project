import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { CovidComponent } from './covid/covid.component';
import { SecurePagesGuard } from './secure-pages.guard';
import { SigninComponent } from './signin/signin.component';
import { CovidCountryComponentComponent } from './covid-country-component/covid-country-component.component';

const routes: Routes = [
  {path: "signin", component: SigninComponent, canActivate: [SecurePagesGuard]},
  {path: "covid", component: CovidComponent, canActivate: [AuthGuard]}, 
  {path: "country/:slug", component: CovidCountryComponentComponent, canActivate: [AuthGuard]},
  {path: "", pathMatch: "full", redirectTo: "signin"},
  {path: "**", redirectTo: "signin"}
];
//canActivate: [SecurePagesGuard] 
//,canActivate: [AuthGuard]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


