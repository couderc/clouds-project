import { User } from "./signin/user.model";

export interface News{
    title : string;
    Date : Date ;
    country : string;
    description : string;
    imageURL : string;
    readMore : string;
    user : User;
}
