import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { CoronavirusService } from '../coronavirus.service';
import { AllCountriesDaily } from '../AllCountriesDaily.model';
import * as moment from 'moment';

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.css']
})
export class LinechartComponent implements OnInit {
  done : Boolean = false;
  allCountriesDaily : AllCountriesDaily;
  lineChartData: ChartDataSets[] = [
    { data: [], label: 'Total Deaths' },
    { data: [], label: 'Total Recovered' },
    { data: [], label: 'Total Cases'}
  ];
  lineChartOptions: ChartOptions = {
    responsive: true,
    plugins: {
      datalabels: {
          display: false,
      },
  }
  };
  lineChartLabels: Label[] = [];
  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType: ChartType = 'line';
  public lineChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(137,196,244,1)', 'rgba(255,246,143,1)'],
    },
  ];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(public coronavirusService: CoronavirusService) { }

  ngOnInit(): void {
    if(!this.done){
      this.coronavirusService.getAllCountriesDaily().subscribe((data: AllCountriesDaily)=>{
        this.allCountriesDaily = data 
        var n = this.allCountriesDaily.Date.length
        if(n > 10){
          for (let j = 0; j < n; j++) {
            this.lineChartData[0].data[j] = this.allCountriesDaily.deaths[j];
            this.lineChartData[1].data[j] = this.allCountriesDaily.recovered[j];
            this.lineChartData[2].data[j] = this.allCountriesDaily.cases[j];
            this.lineChartLabels[j] = moment().subtract('days', n-j).format('MMM Do');
          }
          this.chart.update();
          this.done = true;
        }
      });
    }
  }



  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  
}
