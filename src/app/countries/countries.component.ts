import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CoronavirusService } from '../coronavirus.service';
import { CountriesData } from '../countriesData.model';
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {
  countriesData : CountriesData[] ;
  done : boolean = false;
  
  

  constructor(private router : Router, public coronavirusService : CoronavirusService) { }

  ngOnInit(): void {
    if (!this.done){
      this.coronavirusService.getCountriesData().subscribe((countriesData : CountriesData[]) =>{
          this.countriesData = countriesData;
      this.done = true
      });  
    }
    
  }

  public countryClick(slug : string){
    this.router.navigate(["country/"+ slug]);
    console.log("navigate to country/"+slug);
    this.coronavirusService.apiRequestCountry(slug);
    

    
  }
}
