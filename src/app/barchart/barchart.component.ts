import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { BaseChartDirective, Label } from 'ng2-charts';
import { AllCountriesDaily } from '../AllCountriesDaily.model';
import { CoronavirusService } from '../coronavirus.service';
import * as moment from 'moment';

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.css']
})
export class BarchartComponent implements OnInit {
  done : Boolean = false;
  allCountriesDaily : AllCountriesDaily;

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(137,196,244,1)', 'rgba(255,246,143,1)'],
    },
  ];

  public barChartData: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily Deaths' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily Recovered' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily New Cases' }
  ];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;


  constructor(public coronavirusService: CoronavirusService) { }

  ngOnInit(): void {
    if(!this.done){
      for (let j = 0; j < 7; j++) {
        this.barChartLabels[j] = moment().subtract('days', 6-j).format('MMM Do');
      }
      this.coronavirusService.getAllCountriesDaily().subscribe((data: AllCountriesDaily)=>{
        this.allCountriesDaily = data 
        var n = this.allCountriesDaily.Date.length
        if(n > 10){
          for (let j = 0; j < 7; j++) {
            this.barChartData[0].data[j] = Math.max(this.allCountriesDaily.deaths[n-8+j] - this.allCountriesDaily.deaths[n-9+j],0);
            this.barChartData[1].data[j] = Math.max(this.allCountriesDaily.recovered[n-8+j] - this.allCountriesDaily.recovered[n-9+j],0);
            this.barChartData[2].data[j] = Math.max(this.allCountriesDaily.cases[n-8+j] - this.allCountriesDaily.cases[n-9+j],0);
          }
          this.chart.update();
          this.done = true;
       }
      });
    }
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}
