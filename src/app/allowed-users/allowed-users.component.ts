import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CoronavirusService } from '../coronavirus.service';
import { User } from '../signin/user.model';

@Component({
  selector: 'app-allowed-users',
  templateUrl: './allowed-users.component.html',
  styleUrls: ['./allowed-users.component.css']
})
export class AllowedUsersComponent implements OnInit {
  user : User;
  done : Boolean = false;
  slug : string;
  lebanon : string = "lebanon";
  
  constructor(private router : Router, public coronavirusService: CoronavirusService){}

  ngOnInit(): void { 
    if (!this.done){
      var m = this.router.url.toString().split('/').length
      this.slug = this.router.url.toString().split('/')[m - 1]
      this.user = this.coronavirusService.getUser();
      console.log("1")
      console.log(this.user)
      this.done=true
    }
  }

  addAllowedUsers(){
    this.coronavirusService.addAllowedUsers(this.user);
    console.log("User has been Allowed")
    };

  

}
