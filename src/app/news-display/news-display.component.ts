import { Component, OnInit } from '@angular/core';
import { CoronavirusService } from '../coronavirus.service';
import { News } from '../news.model';

@Component({
  selector: 'app-news-display',
  templateUrl: './news-display.component.html',
  styleUrls: ['./news-display.component.css']
})
export class NewsDisplayComponent implements OnInit {

  newsL: Array<News> = []
  countryTag: string = "worldwild"
  done: boolean = false

  constructor(public coronavirusService: CoronavirusService) { }

  ngOnInit(): void {
    this.coronavirusService.getNews().subscribe((data: News[]) => {
      for(let news of data){
        if(news.country == this.countryTag && !this.done){
          this.newsL.push(news)
        }
      }
      this.done = true
    });
  }


}
