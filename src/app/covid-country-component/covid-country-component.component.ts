import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CoronavirusService } from '../coronavirus.service';
import { CountriesData } from '../countriesData.model';
import { BrowserModule } from '@angular/platform-browser';
import { CovidSummary } from '../CovidSummary.model';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { SpecificDaily } from '../SpecificDaily.model';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import * as moment from 'moment';
import { News } from '../news.model';
import { User } from '../signin/user.model';



@Component({
  selector: 'app-covid-country-component',
  templateUrl: './covid-country-component.component.html',
  styleUrls: ['./covid-country-component.component.css']
})
export class CovidCountryComponentComponent implements OnInit {
  done : Boolean = false;
  user : User;
  slug: string;
  specificDaily: SpecificDaily[];
  covidSummary: CovidSummary;
  dCountry: Array<string> = [];
  dDeaths: Array<number> = [];
  dRecovered: Array<number> = [];
  dConfirmed: Array<number> = [];
  Deaths: Array<number> = [];
  Recovered: Array<number> = [];
  Confirmed: Array<number> = [];
  dDate: Array<string> = [];

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = [['Dead Cases'], ['Recovered Cases'], ['Active Cases']];
  public pieChartData: number[] = [0, 500, 100];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(137,196,244,1)', 'rgba(255,246,143,1)'],
    },
  ];

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily Deaths' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily Recovered' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily New Cases' }
  ];

  lineChartData: ChartDataSets[] = [
    { data: [], label: 'Total Deaths' },
    { data: [], label: 'Total Recovered' },
    { data: [], label: 'Total Cases'}
  ];
  lineChartOptions: ChartOptions = {
    responsive: true,
    plugins: {
      datalabels: {
          display: false,
      },
  }
  };
  lineChartLabels: Label[] = [];
  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType: ChartType = 'line';

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;


  constructor(private router: Router, public coronavirusService: CoronavirusService) { }

  ngOnInit(): void {
    
    if(!this.done){
      this.user = this.coronavirusService.getUser();
      var m = this.router.url.toString().split('/').length
      this.slug = this.router.url.toString().split('/')[m - 1]
      console.log(this.slug)
            for (let j = 0; j < 7; j++) {
        this.barChartLabels[j] = moment().subtract('days', 6-j).format('MMM Do');
      }
      this.coronavirusService.getSpecificDaily(this.slug).subscribe((data: SpecificDaily[])=>{
        this.specificDaily = data 
        if(this.specificDaily.length > 100){
          var n = this.specificDaily.length
          for (let j = 0; j < 7; j++) {
            this.barChartData[0].data[j] = Math.max(this.specificDaily[n-8+j].Deaths - this.specificDaily[n-9+j].Deaths,0);
            this.barChartData[1].data[j] = Math.max(this.specificDaily[n-8+j].Recovered - this.specificDaily[n-9+j].Recovered,0);
            this.barChartData[2].data[j] = Math.max(this.specificDaily[n-8+j].Confirmed - this.specificDaily[n-9+j].Confirmed,0);
          }
          
          for (let j = 0; j < this.specificDaily.length; j++) {
            this.lineChartData[0].data[j] = this.specificDaily[j].Deaths;
            this.lineChartData[1].data[j] = this.specificDaily[j].Recovered;
            this.lineChartData[2].data[j] = this.specificDaily[j].Confirmed;
            this.lineChartLabels[j] = moment().subtract('days', this.specificDaily.length-j).format('MMM Do');
          }

          this.chart.update();
        }
      })

      this.coronavirusService.getCountriesData().subscribe((countriesData: CountriesData[]) => {
        countriesData.forEach((country) => {
          if (country.Slug == this.slug) {
            this.covidSummary = {
              totalCases: country.TotalConfirmed,
              newCases: country.NewConfirmed,
              activeCases: country.TotalConfirmed - country.TotalDeaths - country.TotalRecovered,
              totalRecovered: country.TotalRecovered,
              newRecovered: country.NewRecovered,
              recoveryRate: Math.round((10000 * country.TotalRecovered / country.TotalConfirmed)) / 100,
              totalDeaths: country.TotalDeaths,
              newDeaths: country.NewDeaths,
              mortalityRate: Math.round((10000 * country.TotalDeaths / country.TotalConfirmed)) / 100,
              Date: new Date(country.Date),
            }
          }
        });
      });
      this.done = true
    }
  }
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
  
}



  

  
  

 


  

  

  

  
