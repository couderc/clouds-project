import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidCountryComponentComponent } from './covid-country-component.component';

describe('CovidCountryComponentComponent', () => {
  let component: CovidCountryComponentComponent;
  let fixture: ComponentFixture<CovidCountryComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CovidCountryComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidCountryComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
