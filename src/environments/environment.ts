// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDZZXLo2PTw9UYC_T_p6I7irshXevAPCAs",
    authDomain: "coronavirus-a3696.firebaseapp.com",
    databaseURL: "https://coronavirus-a3696.firebaseio.com",
    projectId: "coronavirus-a3696",
    storageBucket: "coronavirus-a3696.appspot.com",
    messagingSenderId: "664073588393",
    appId: "1:664073588393:web:7a152b335687e5482f63b8",
    measurementId: "G-8SE95X1YJF"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
